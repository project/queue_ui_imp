<?php

/**
 * @file
 * Drush support for the migrate module
 */

/**
 * Implements hook_drush_command().
 */
function queue_ui_drush_command() {

  $items['queue-ui-batch-run'] = array(
    'description' => 'Run batch process for a given queue.',
    'arguments' => array(
      'queue_name' => 'Restrict to a single queue. Optional',
    ),
    'examples' => array(
      'queue-ui-batch-run' => 'Run batch process for all queues',
      'queue-ui-batch-run custom_queue' => 'Run batch process for just one queue',
    ),
    'drupal dependencies' => array('queue_ui'),
    'aliases' => array('queue-batch', 'qui-batch', 'quib'),
  );

  return $items;
}

/**
 * Get/Filter queues for the batch process.
 *
 * @param $content_set
 *  The name of the Queue
 */
function drush_queue_ui_get_queues($args = NULL) {

  $queue_objects = queue_ui_defined_queues();

  if ($args) {
    $named_queues = array();
    foreach (explode(',', $args) as $name) {
      $found = FALSE;
      foreach ($queue_objects as $machine_name => $queue) {
        if (drupal_strtolower($name) == drupal_strtolower($machine_name)) {
          $found = TRUE;
          $named_queues[$machine_name] = $queue;
        }
      }
      if (!$found) {
        drush_log(dt('No queue with machine name !name found', array('!name' => $name)), 'error');
      }
    }
    $queue_objects = $named_queues;
  }

  return $queue_objects;
}

/**
 * Run batch process for a given queue.
 *
 * @param $content_set
 *  The name of the Queue
 */
function drush_queue_ui_batch_run($args = NULL) {

  $defined_queues = array();
  $defined_queues = drush_queue_ui_get_queues($args);

  foreach ($defined_queues as $name => $queue) {
    $batch = $defined_queues[$name]['batch'];
    // Add queue as argument to operations by resetting the operations array.
    $operations = array();
    $queue = DrupalQueue::get($name);
    foreach ($batch['operations'] as $operation) {
      // First element is the batch process callback, second is the argument.
      $operations[] = array($operation[0], array_merge(array($queue), $operation[1]));
    }
    $batch['operations'] = $operations;
    $batch['file'] = drupal_get_path('module', 'queue_ui') . '/queue_ui.module';
    // Set the batch process.
    $batch['progressive'] = FALSE;
    batch_set($batch);
    drush_log(dt("Starting '!name' queue batch process.", array('!name' => $name)), 'status');
    drush_backend_batch_process();
  }
}
